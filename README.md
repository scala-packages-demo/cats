# cats

[typelevel/cats](https://index.scala-lang.org/typelevel/cats) Functional programming

![Cats Friendly Badge](https://typelevel.org/cats/img/cats-badge-tiny.png)

Typelevel.scala [typelevel.org](https://typelevel.org)

# About Reader Monad
* [scala Reader Monad
  ](https://www.google.com/search?q=scala+Reader+Monad)
* [*Dependency injection with Reader Monad in Scala*
  ](https://medium.com/rahasak/dependency-injection-with-reader-monad-in-scala-fe05b29e04dd)
  2020-01 λ.eranga
